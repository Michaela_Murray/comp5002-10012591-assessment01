﻿using System;

namespace comp5002_10012591_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
           
            //Welcome people to the program
            Console.WriteLine("What is your name?");
            var name = (Console.ReadLine());
            Console.WriteLine($"Welcome {name} to the store program");
           
            //number variables for the "client" 
            Console.WriteLine("Please enter a number with 2 decimal places:");
            var num1 = double.Parse(Console.ReadLine());
            Console.WriteLine($"The number you have entered is {num1}");
            
            //if statement for adding new numbers into the equation
            Console.WriteLine("Do you wish to add another number?");
            var answer = Console.ReadLine();
            

            if (answer == "yes")
            {
                    Console.WriteLine("Please enter the new number you wish to add");
                    num1 += double.Parse(Console.ReadLine());
                    Console.WriteLine($"Your total comes to {(num1)}");
            }
            else
            {
                Console.WriteLine($"Your total comes to {(num1)}");
            }
           
            //add GST to total of price given by "client"
            Console.WriteLine($"Your total cost, with GST is {((num1 * 0.15) + num1)}");
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Thank you for visiting the store! Please come again");
            Console.ReadKey();
        }

        private static int NewMethod()
        {
            return 15;
        }
    }
}
